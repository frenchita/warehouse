require('dotenv').config()
const express = require('express')
const app = express()
require('./config/mysql')

app.get('/', (req, res) => {
    res.send({
        msj: 'Hola Mundo :-)'
    })
})

app.use(express.json())
app.use(express.urlencoded({
    extended: true
}))



require('./routes/regions')
var regions = require('./routes/regions');
app.use('/regions', regions);


app.listen(process.env.PORT, ()=>{
    console.log(`Server started in port ${process.env.PORT}`);
})



console.log(process.env.port)