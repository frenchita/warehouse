const express = require('express');
const router = express.Router();
const auth = require('../middleware/auth');

const regions = require('../data/regions');

//con middleware
router.get('/', auth, function(req, res) {
    regions.get().then((regionsData)=>{
        res.json(regionsData);
    });
});

router.post('/', function(req, res) {
    regions.add(req).then((regionsData)=>{
        res.json(regionsData);
    });
});

router.put('/', function(req, res) {
    regions.update(req).then((regionsData)=>{
        res.json(regionsData);
    });
});

router.delete('/', function(req, res) {

    //validar relaciones antes de eliminar la región

    regions.remove(req).then((regionsData)=>{
        res.json(regionsData);
    });
});

module.exports = router;